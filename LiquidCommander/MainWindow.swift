//
//  ViewController.swift
//  TerminalMate
//
//  Created by Sascha Lamprecht on 23.05.20.
//  Copyright © 2020 bashmate.com. All rights reserved.
//

import Cocoa

class MainWindow: NSViewController {
    
    @IBOutlet weak var fading_from: NSColorWell!
    @IBOutlet var output_window: NSTextView!
    
    @IBOutlet weak var liquidctl_status: NSImageView!
    @IBOutlet weak var button_read: NSButton!

    @IBOutlet weak var slider_pump_speed: NSSlider!
    @IBOutlet weak var slider_fan1_speed: NSSlider!
    @IBOutlet weak var slider_fan2_speed: NSSlider!
    @IBOutlet weak var slider_fan3_speed: NSSlider!
    
    @IBOutlet weak var percent_pump_speed: NSTextField!
    @IBOutlet weak var percent_fan1_speed: NSTextField!
    @IBOutlet weak var percent_fan2_speed: NSTextField!
    @IBOutlet weak var percent_fan3_speed: NSTextField!
    
    @IBOutlet weak var percent_symbol1: NSTextField!
    @IBOutlet weak var percent_symbol2: NSTextField!
    @IBOutlet weak var percent_symbol3: NSTextField!
    @IBOutlet weak var percent_symbol4: NSTextField!
    
    @IBOutlet weak var model: NSTextField!
    
    @IBOutlet weak var textfield_liquid_temperature: NSTextField!
    @IBOutlet weak var textfield_fan1_speed: NSTextField!
    @IBOutlet weak var textfield_fan2_speed: NSTextField!
    @IBOutlet weak var textfield_fan3_speed: NSTextField!
    @IBOutlet weak var textfield_pump_speed: NSTextField!
    @IBOutlet weak var textfield_firmware_version: NSTextField!
    
    
    let scriptPath = Bundle.main.path(forResource: "/Script/script", ofType: "command")!
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

    let userDesktopDirectory:String = NSHomeDirectory()
    
    override func viewDidAppear() {
        super.viewDidAppear()
        self.view.window?.title = "Liquid Commander v" + appVersion!

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.removeObject(forKey: "Error")
        UserDefaults.standard.removeObject(forKey: "Model")
        UserDefaults.standard.removeObject(forKey: "Number of fans")
        UserDefaults.standard.set("n/a", forKey: "Liquid temperature")
        UserDefaults.standard.set("n/a", forKey: "Pump speed")
        UserDefaults.standard.set("n/a", forKey: "Firmware version")
        UserDefaults.standard.set("n/a", forKey: "Fan1 speed")
        UserDefaults.standard.set("n/a", forKey: "Fan2 speed")
        UserDefaults.standard.set("n/a", forKey: "Fan3 speed")
        
        self.syncShellExec(path: self.scriptPath, args: ["clean"])
        self.syncShellExec(path: self.scriptPath, args: ["initial"])
        self.syncShellExec(path: self.scriptPath, args: ["check_liquidctl"])
        
        let check_liquidctl = UserDefaults.standard.bool(forKey: "liquidctl installed")
        if check_liquidctl == true {
            liquidctl_status.image = NSImage(named: "NSStatusAvailable")
            self.button_read.isEnabled = true
        } else {
            liquidctl_status.image = NSImage(named: "NSStatusUnavailable")
            no_liquidctl ()
        }
        
        let fontsize = CGFloat(15)
        let fontfamily = "Courier"
        output_window.font = NSFont(name: fontfamily, size: fontsize)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func read(_ sender: Any) {
        output_window.textStorage?.mutableString.setString("")
        DispatchQueue.global(qos: .background).async {
            self.syncShellExec(path: self.scriptPath, args: ["start"])
            DispatchQueue.main.async {
                let error = UserDefaults.standard.string(forKey: "Error")
                    if error == "no_match" {
                        self.no_match()
                   } else {
                        self.textfield_liquid_temperature.isEnabled = true
                        self.textfield_firmware_version.isEnabled = true
                        
                        let number_of_fans = UserDefaults.standard.string(forKey: "Number of fans")
                        
                        if number_of_fans == "1" {
                            if !(self.textfield_fan1_speed.stringValue=="n/a") {
                                    self.textfield_fan1_speed.isEnabled = true
                                    self.slider_fan1_speed.isHidden = false
                                    self.percent_fan1_speed.isHidden = false
                                    self.percent_symbol1.isHidden = false
                            }
                        }
                        
                        if number_of_fans == "2" {
                            if !(self.textfield_fan1_speed.stringValue=="n/a") {
                                    self.textfield_fan1_speed.isEnabled = true
                                    self.slider_fan1_speed.isHidden = false
                                    self.percent_fan1_speed.isHidden = false
                                    self.percent_symbol1.isHidden = false
                            }
                            if !(self.textfield_fan2_speed.stringValue=="n/a") {
                                    self.textfield_fan2_speed.isEnabled = true
                                    self.slider_fan2_speed.isHidden = false
                                    self.percent_fan2_speed.isHidden = false
                                    self.percent_symbol2.isHidden = false
                            }
                        }
                        
                        if number_of_fans == "3" {
                            if !(self.textfield_fan1_speed.stringValue=="n/a") {
                                    self.textfield_fan1_speed.isEnabled = true
                                    self.slider_fan1_speed.isHidden = false
                                    self.percent_fan1_speed.isHidden = false
                                    self.percent_symbol1.isHidden = false
                            }
                            if !(self.textfield_fan2_speed.stringValue=="n/a") {
                                    self.textfield_fan2_speed.isEnabled = true
                                    self.slider_fan2_speed.isHidden = false
                                    self.percent_fan2_speed.isHidden = false
                                    self.percent_symbol2.isHidden = false
                            }
                            if !(self.textfield_fan3_speed.stringValue=="n/a") {
                                    self.textfield_fan3_speed.isEnabled = true
                                    self.slider_fan3_speed.isHidden = false
                                    self.percent_fan3_speed.isHidden = false
                                    self.percent_symbol3.isHidden = false
                            }
                        }
                        
                        if !(self.textfield_pump_speed.stringValue.isEmpty) {
                                self.textfield_pump_speed.isEnabled = true
                                self.slider_pump_speed.isHidden = false
                                self.percent_pump_speed.isHidden = false
                                self.percent_symbol4.isHidden = false
                        }
                         
                        // Determine the file name
                        let filename = NSHomeDirectory() + "/.lq_temp/status"
                        // Read the contents of the specified file
                        let contents = try! String(contentsOfFile: filename)
                        // Split the file into separate lines
                        let lines = contents.split(separator:"\n")
                        // Iterate over each line and print the line
                        for line in lines {
                            self.model.stringValue="\(line)"
                            break
                        }
                        }
                }
        }
    }

 
    @IBAction func change_fan1_speed(_ sender: Any) {
        //output_window.textStorage?.mutableString.setString("")
        DispatchQueue.global(qos: .background).async {
            self.syncShellExec(path: self.scriptPath, args: ["change_fan1_speed"])
            DispatchQueue.main.async {
            }
        }
    }
    
    @IBAction func change_fan2_speed(_ sender: Any) {
        //output_window.textStorage?.mutableString.setString("")
        DispatchQueue.global(qos: .background).async {
            self.syncShellExec(path: self.scriptPath, args: ["change_fan2_speed"])
            DispatchQueue.main.async {
            }
        }
    }
    
    @IBAction func change_fan3_speed(_ sender: Any) {
        //output_window.textStorage?.mutableString.setString("")
        DispatchQueue.global(qos: .background).async {
            self.syncShellExec(path: self.scriptPath, args: ["change_fan3_speed"])
            DispatchQueue.main.async {
            }
        }
    }
    
    @IBAction func change_pump_speed(_ sender: Any) {
       //output_window.textStorage?.mutableString.setString("")
        DispatchQueue.global(qos: .background).async {
            self.syncShellExec(path: self.scriptPath, args: ["change_pump_speed"])
            DispatchQueue.main.async {
            }
        }
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
       self.syncShellExec(path: self.scriptPath, args: ["clean"])
    }

    func no_match (){
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("No device matches available drivers and selection criteria!", comment: "")
        alert.informativeText = NSLocalizedString("Is the AIO connected?", comment: "")
        alert.alertStyle = .warning
        let Button = NSLocalizedString("Bummer", comment: "")
        alert.addButton(withTitle: Button)
        alert.runModal()
    }
    
    func no_devices_found (){
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("No device found!", comment: "")
        alert.informativeText = NSLocalizedString("Is the AIO connected?", comment: "")
        alert.alertStyle = .warning
        let Button = NSLocalizedString("Bummer", comment: "")
        alert.addButton(withTitle: Button)
        alert.runModal()
    }
    
    func no_liquidctl (){
        let alert = NSAlert()
        alert.messageText = NSLocalizedString("'liquidctl' not found!", comment: "")
        alert.informativeText = NSLocalizedString("Please install with 'brew install liquidctl' before", comment: "")
        alert.alertStyle = .warning
        let Button = NSLocalizedString("OK", comment: "")
        alert.addButton(withTitle: Button)
        alert.runModal()
    }
    
    func syncShellExec(path: String, args: [String] = []) {
        let process            = Process()
        process.launchPath     = "/bin/bash"
        process.arguments      = [path] + args
        let outputPipe         = Pipe()
        let filelHandler       = outputPipe.fileHandleForReading
        process.standardOutput = outputPipe
        
        let group = DispatchGroup()
        group.enter()
        filelHandler.readabilityHandler = { pipe in
            let data = pipe.availableData
            if data.isEmpty { // EOF
                filelHandler.readabilityHandler = nil
                group.leave()
                return
            }
            if let line = String(data: data, encoding: String.Encoding.utf8) {
                DispatchQueue.main.sync {
                    self.output_window.string += line
                    self.output_window.scrollToEndOfDocument(nil)
                }
            } else {
                print("Error decoding data: \(data.base64EncodedString())")
            }
        }
        process.launch() // Start process
        process.waitUntilExit() // Wait for process to terminate.
    }

}

