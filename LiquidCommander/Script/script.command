#!/bin/bash

ScriptHome=$(echo $HOME)
ScriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
plist="${ScriptHome}/Library/Preferences/liquidcommander.slsoft.de"
liquidctl="/usr/local/bin/./liquidctl"

function initial()
{
    mkdir "${ScriptHome}"/.lq_temp
}

function check_liquidctl()
{
    if [ -f /usr/local/bin/liquidctl ]; then
        defaults write "$plist" "liquidctl installed" -bool true
    else
        defaults write "$plist" "liquidctl installed" -bool false
    fi
}

function start()
{
    $liquidctl initialize > "${ScriptHome}"/.lq_temp/status
    if [[ "$?" != "0" ]]; then
        defaults write "$plist" "Error" "no_match"
        exit
    fi
    $liquidctl status >> "${ScriptHome}"/.lq_temp/status
    if [[ "$?" != "0" ]]; then
        defaults write "$plist" "Error" "no_match"
        exit
    fi
    
    status=$( cat "${ScriptHome}"/.lq_temp/status |sed -e 's/(experimental)//g' -e 's/.*─\ //g' |uniq )

    if echo -e "$status" |grep -w "Fan" > /dev/null
    then
        defaults write "$plist" "Number of fans" "1"
    fi

    if echo -e "$status" |grep -w "Fan 1" > /dev/null
    then
        defaults write "$plist" "Number of fans" "1"
    fi

    if echo -e "$status" |grep -w "Fan 2" > /dev/null
    then
        defaults write "$plist" "Number of fans" "2"
    fi

    if echo -e "$status" |grep -w "Fan 3" > /dev/null
    then
        defaults write "$plist" "Number of fans" "3"
    fi
    
    if echo -e "$status" |grep -w "Firmware version" > /dev/null
    then
        firmware_version=$( echo -e "$status" |grep -w "Firmware version" |sed 's/.*ion//g' |xargs )
        defaults write "$plist" "Firmware version" "$firmware_version"
        #echo -e "Firmware version\t\t" "$firmware_version"
    fi

    if echo -e "$status" |grep -w "Liquid temperature" > /dev/null
    then
        liquid_temperature=$( echo -e "$status" |grep -w "Liquid temperature" |sed 's/.*ure//g' |xargs )
        defaults write "$plist" "Liquid temperature" "$liquid_temperature"
        #echo -e "Liquid temperature\t\t" "$liquid_temperature"
    fi
    
    if echo -e "$status" |grep -w "Fan speed" > /dev/null
    then
        fan1_speed=$( echo -e "$status" |grep -w "Fan speed" |sed -e 's/.*eed//g' -e 's/rpm.*//g' |xargs )
        defaults write "$plist" "Fan1 speed" "$fan1_speed"
        #echo -e "Fan speed\t\t\t\t\t" "$fan_speed"
    fi
    
    if echo -e "$status" |grep -w "Fan 1 speed" > /dev/null
    then
        fan1_speed=$( echo -e "$status" |grep -w "Fan 1 speed" |sed -e 's/.*eed//g' -e 's/rpm.*//g' |xargs )
        defaults write "$plist" "Fan1 speed" "$fan1_speed"
        #echo -e "Fan speed\t\t\t\t\t" "$fan_speed"
    fi
    
    if echo -e "$status" |grep -w "Fan 2 speed" > /dev/null
    then
        fan2_speed=$( echo -e "$status" |grep -w "Fan 2 speed" |sed -e 's/.*eed//g' -e 's/rpm.*//g' |xargs )
        defaults write "$plist" "Fan2 speed" "$fan2_speed"
        #echo -e "Fan speed\t\t\t\t\t" "$fan_speed"
    fi

    if echo -e "$status" |grep -w "Fan 3 speed" > /dev/null
    then
        fan3_speed=$( echo -e "$status" |grep -w "Fan 3 speed" |sed -e 's/.*eed//g' -e 's/rpm.*//g' |xargs )
        defaults write "$plist" "Fan3 speed" "$fan3_speed"
        #echo -e "Fan speed\t\t\t\t\t" "$fan_speed"
    fi

    if echo -e "$status" |grep -w "Pump speed" > /dev/null
    then
        pump_speed=$( echo -e "$status" |grep -w "Pump speed" |sed -e 's/.*eed//g' -e 's/rpm.*//g' |xargs )
        defaults write "$plist" "Pump speed" "$pump_speed"
        #echo -e "Pump speed\t\t\t\t" "$pump_speed"
    fi
    
    if echo -e "$status" |grep -w "Pump Logo LEDs" > /dev/null
    then
        pump_logo_leds=$( echo -e "$status" |grep -w "Pump Logo LEDs" |sed 's/.*LEDs//g' |xargs )
        defaults write "$plist" "Pump Logo LEDs" "$pump_logo_leds"
        echo -e "Pump Logo LEDs\t\t\t" "$pump_logo_leds"
    fi
    
    if echo -e "$status" |grep -w "Pump Ring LEDs" > /dev/null
    then
        pump_ring_leds=$( echo -e "$status" |grep -w "Pump Ring LEDs" |sed 's/.*LEDs//g' |xargs )
        defaults write "$plist" "Pump Ring LEDs" "$pump_ring_leds"
        echo -e "Pump Ring LEDs\t\t\t" "$pump_ring_leds"
    fi

    if echo -e "$status" |grep -w "Pump duty" > /dev/null
    then
        pump_duty=$( echo -e "$status" |grep -w "Pump duty" |sed 's/.*duty//g' |xargs )
        defaults write "$plist" "Pump duty" "$pump_duty"
        echo -e "Pump duty\t\t\t\t\t" "$pump_duty"
    fi

}

function change_pump_speed()
{
    pump_speed=$( defaults read "$plist" "Pump speed set" |sed 's/,.*//g')
    $liquidctl set pump speed "$pump_speed"
    echo -e "Pump speed set to ""$pump_speed""%"
    start > /dev/null
    
}

function change_fan1_speed()
{
    fan1_speed=$( defaults read "$plist" "Fan1 speed set" |sed 's/,.*//g')
    $liquidctl set fan speed "$fan1_speed"
    echo -e "Fan #1 speed set to ""$fan1_speed""%"
    start > /dev/null
    
}

function change_fan2_speed()
{
    fan2_speed=$( defaults read "$plist" "Fan2 speed set" |sed 's/,.*//g')
    $liquidctl set fan2 speed "$fan2_speed"
    echo -e "Fan #2 speed set to ""$fan2_speed""%"
    start > /dev/null
    
}

function change_fan3_speed()
{
    fan3_speed=$( defaults read "$plist" "Fan3 speed set" |sed 's/,.*//g')
    $liquidctl set fan3 speed "$fan3_speed"
    echo -e "Fan #3 speed set to ""$fan3_speed""%"
    start > /dev/null
    
}

function clean()
{
    rm -rf "${ScriptHome}"/.lq_temp
}


$1
