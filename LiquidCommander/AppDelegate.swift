//
//  AppDelegate.swift
//  TerminalMate
//
//  Created by Sascha Lamprecht on 23.05.20.
//  Copyright © 2020 bashmate.com. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    let scriptPath = Bundle.main.path(forResource: "/Script/script", ofType: "command")!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        self.syncShellExec(path: self.scriptPath, args: ["clean"])
    }

    func syncShellExec(path: String, args: [String] = []) {
        let process            = Process()
        process.launchPath     = "/bin/bash"
        process.arguments      = [path] + args
        process.launch() // Start process
        process.waitUntilExit() // Wait for process to terminate.
    }

}

